package mx.uach.fing.transportes.models.aereos;

/**
 * Modelo de un Helicoptero que hereda
 * sus propiedades de {@link Aereo} dentro del
 * sistema.
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 * @see Aereo
 */


public class Helicoptero extends Aereo {

    public Helicoptero(){

    }
}
