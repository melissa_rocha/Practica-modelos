package mx.uach.fing.transportes.models.terrestres;

/**
 * Modelo de un Tren que hereda sus propiedades de Terrestre
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 * @see Terrestre
 */

public class Tren extends Terrestre {

    public Tren(){

    }
}
