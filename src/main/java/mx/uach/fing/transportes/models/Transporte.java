package mx.uach.fing.transportes.models;

import mx.uach.fing.transportes.models.Interfaces.Status;
import mx.uach.fing.transportes.models.Interfaces.annotations.Table;
import mx.uach.fing.transportes.models.terrestres.Automovil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Modelo de un Transporte en el sistema, es la clase padre de todos los modelos
 * en los Transportes
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 */

public class Transporte {

    private Integer numeroRuedas;
    private Integer numeroPuertas;
    private Double altura;
    private String tamaño;

    public Transporte(){

        this.numeroRuedas = 4;
        this.numeroPuertas = 4;
        this.tamaño = "Grande";
        this.altura = 0.01d;

    }


    public static void main (String[] args){

        Transporte transporte = new Transporte();

        Integer resultado = transporte.formula(1540, 50);

        Automovil auto = new Automovil(){

            @Override
            public void gasolina() {
                System.out.println("Solo tienes medio tanque, cuidado");
            }
        };

        List<Status> status = new ArrayList<>();
        status.add(auto);

        status.stream().forEach(s -> {
            s.enceder();
            s.arrancar();
            s.gasolina();
        });

        System.out.println(String.format("La velocidad es: %d", resultado));

        Table m = Automovil.class.getAnnotation(Table.class);
        if(null != m){
            System.out.println();
            System.out.println("PROPIEDADES DEL TRANSPORTE");
            System.out.println("m.tipo = " + m.tipo());
            System.out.println("m.marca = " + m.marca());
            System.out.println("m.color = " + m.color());
            System.out.println("m.estilo = " + m.estilo());
        }
    }


    public Integer formula (Integer distancia, Integer tiempo){

    List<Integer> calculos = Arrays.asList(distancia);

    return calculos.stream().mapToInt(calculo -> calculo / tiempo).sum();

    }

}
