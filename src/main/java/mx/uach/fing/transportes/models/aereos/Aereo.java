package mx.uach.fing.transportes.models.aereos;

import mx.uach.fing.transportes.models.Transporte;

/**
 * Modelo de un Transporte Aereo que hereda sus propiedades de Transporte
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 * @see Transporte
 */

public class Aereo extends Transporte {

    public Aereo(){


    }
}
