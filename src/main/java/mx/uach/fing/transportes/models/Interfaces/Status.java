package mx.uach.fing.transportes.models.Interfaces;

public interface Status {

    public void enceder();
    public void arrancar();
    public void gasolina();
}
