package mx.uach.fing.transportes.models.terrestres;

import mx.uach.fing.transportes.models.Interfaces.Status;
import mx.uach.fing.transportes.models.Interfaces.annotations.Table;

/**
 * Modelo de un Automovil que hereda
 * sus propiedades de {@link Terrestre} dentro del
 * sistema.
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 * @see Terrestre
 */

@Table(tipo = "Automovil", marca = "Toyota", color = "Azul", estilo = "Normal")
public abstract class Automovil extends Terrestre implements Status{

    @Override
    public void enceder() {

        System.out.println("Estoy prendido");

    }

    @Override
    public void arrancar() {
        System.out.println("Estoy avanzando");
    }
}
