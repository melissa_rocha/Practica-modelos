package mx.uach.fing.transportes.models.aereos;

/**
 * Modelo de un Avion que hereda
 * sus propiedades de {@link Aereo} dentro del
 * sistema.
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 * @see Aereo
 */

public class Avion extends Aereo {

    public Avion(){


    }
}
