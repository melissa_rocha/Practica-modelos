package mx.uach.fing.transportes.models.acuaticos;


/**
 * Modelo de una Lancha que hereda
 * sus propiedades de {@link Acuatico} dentro del
 * sistema.
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 * @see Acuatico
 */


public class Lancha extends Acuatico {

    public Lancha(){

    }
}
