package mx.uach.fing.transportes.models.terrestres;

/**
 * Modelo de un Autobus que hereda
 * sus propiedades de {@link Terrestre} dentro del
 * sistema.
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 * @see Terrestre
 */

public class Autobus extends Terrestre {

    public Autobus(){

    }
}
