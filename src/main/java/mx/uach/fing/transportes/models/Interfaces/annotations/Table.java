package mx.uach.fing.transportes.models.Interfaces.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)

public @interface Table {

    String tipo() default "Automovil";
    String marca() default "Nissan";
    String color() default "Rojo";
    String estilo() default "Clasico";

}
