package mx.uach.fing.transportes.models.terrestres;

/**
 * Modelo de una Bicicleta que hereda
 * sus propiedades de {@link Terrestre} dentro del
 * sistema.
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 * @see Terrestre
 */


public class Bicicleta extends Terrestre{

    public Bicicleta(){


    }
}
