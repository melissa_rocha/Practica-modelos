package mx.uach.fing.transportes.models.acuaticos;


/**
 * Modelo de un Submarino que hereda
 * sus propiedades de {@link Acuatico} dentro del
 * sistema.
 *
 * @author Melissa Rocha
 * @since 1.0
 * @version 1.0
 * @see Acuatico
 */


public class Submarino extends Acuatico {

    public Submarino(){


    }
}
